module gitlab.com/abhishp/rain-csv-parser

go 1.16

require (
	github.com/dimchansky/utfbom v1.1.1
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.21.0
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.7.0
	github.com/urfave/cli/v2 v2.3.0
)

replace gitlab.com/abhishp/rain-csv-parser => ./
