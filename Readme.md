# Rain CSV Parser

## Problem statement

Create a CSV parser to parse the data from different employers to create a list of eligible employees to sign up for
Rain. This data needs to be sanitized and standardized as this data and name for fields in the CSV is controlled by the
employers and needs to be normalized to be processed further.

### Functional Requirements

1. Parse files
2. Flag any data problems.
3. Output a summary in the console
4. Generate two csv files, one for correct data, one for bad data.

### Required Data

- Employee name
- Employee salary
- Employee email
- Employee ID
- Emails must be unique

## Usage

```shell
rain-csv-parser process - process the csv file(s)

USAGE:
   rain-csv-parser process [options] input.csv [input.csv...]

OPTIONS:
   --config, -c config-file.yml   use the specified config file (default: "parser.yml") ENV[$RAIN_CSV_PARSER_CONFIG]
   --help, -h                     show help
   --debug, -d, -v                enable debug output (default: false)
   --success, -s succes-file.csv  set the success output file path (default: "success.csv")
   --error, -e error-file.csv     set the error output file path (default: "errors.csv")
   --error-dir, --ed error-dir    set the error output directory path when processing multiple files (default: "errors")
```

## Examples

```shell
#run for single csv input
rain-csv-parser process test/.fixtures/csv/roster1.csv


#run for multiple csv inputs
rain-csv-parser process test/.fixtures/csv/roster1.csv test/.fixtures/csv/roster1.csv
rain-csv-parser process test/.fixtures/csv/roster*.csv

#run with options
rain-csv-parser -c sample.parser.yml test/.fixtures/csv/roster1.csv
rain-csv-parser -d -c sample.parser.yml -s rain.csv -e rain.errors.csv test/.fixtures/csv/roster1.csv
rain-csv-parser -c sample.parser.yml -s rain.csv -ed rain-csv-errors test/.fixtures/csv/roster*.csv
```

## Assumptions

- All the CSV values needs to be space trimmed.
- When processing multiple files, uniqueness constraint is applied on all the records.
- The order of records needs not to be maintained.

## Tech Stack

- make
- Golang 1.16+

## Development

### Pre Requisites

- Install make using your favourite package manager
- Install and setup go 1.16+ using the following [link](https://golang.org/doc/install)
  - Make sure that the executable `go` is in your shell's path.

### Setup

- To set up the local config file and fetch dependencies
  ```shell
  make setup-local
  ```
- To create a binary on local run following command at the root of the directory containing source code
  ```shell
  make build
  ```
  - This would create a binary named `rain-csv-parser[.exe]` in the `build/out` directory

#### Linting

```shell
make lint
```

### Testing

- To run tests locally execute the following command:
    ```shell
    make test
    ```

## Future Improvements

- Make space trimming for values optional.
- Add integration/end-to-end tests.
- Add default currency config to format money type.
- Add multiple constraints for one attribute.
- Add concrete error types wherever possible.
- Move logic in main/processor.go to processor package and add tests for the same.
- Publish metrics to a metric store like InfluxDB.
