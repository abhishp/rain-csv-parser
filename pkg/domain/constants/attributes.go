package constants

const (
	AttributeTypeEmailID = "emailId"
	AttributeTypeMoney   = "money"
	AttributeTypeString  = "string"
)
