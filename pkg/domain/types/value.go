package types

import "fmt"

type Unmarshaler interface {
	UnmarshalText(value string) error
}

type Validator interface {
	Validate() error
}

type Value interface {
	Unmarshaler
	Validator
	fmt.Stringer
}
