package types

type valueInitFunc func() Value

var allTypes = map[string]valueInitFunc{}

func ValueFor(valueType string) Value {
	return allTypes[valueType]()
}

func registerInitFunc(valueType string, initFunc valueInitFunc) {
	allTypes[valueType] = initFunc
}
