package types

import (
	"errors"
	"strings"

	"gitlab.com/abhishp/rain-csv-parser/pkg/domain/constants"
)

func init() {
	registerInitFunc(constants.AttributeTypeString, func() Value {
		return new(String)
	})
}

type String struct {
	value string
}

func (s *String) UnmarshalText(value string) error {
	s.value = strings.TrimSpace(value)
	return nil
}

func (s *String) String() string {
	return s.value
}

func (s *String) Validate() error {
	if s == nil || s.value == "" {
		return errors.New("string value can not be empty")
	}
	return nil
}
