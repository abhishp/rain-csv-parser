package types_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"

	"gitlab.com/abhishp/rain-csv-parser/pkg/domain/types"
)

type EmailIDTestSuite struct {
	suite.Suite
}

func TestEmailID(t *testing.T) {
	suite.Run(t, new(EmailIDTestSuite))
}

func (suite *EmailIDTestSuite) TestEmailID_UnmarshalText() {
	suite.Run("return no error", func() {
		testCases := map[string]struct {
			email    string
			expected string
		}{
			"empty value":                  {email: "", expected: ""},
			"value with just white spaces": {email: " \t    ", expected: ""},
			"valid email":                  {email: "abc@test.com", expected: "abc@test.com"},
		}

		for title, testCase := range testCases {
			suite.Run(title, func() {
				emailID := new(types.EmailID)

				err := emailID.UnmarshalText(testCase.email)

				suite.Require().Nil(err)
				suite.Equal(testCase.expected, emailID.String())
			})
		}
	})

	suite.Run("return error for invalid email address", func() {
		testCases := map[string]string{
			"starting with non-alphanumeric character": "*abc@test.com",
			"special character in local part":          "ab#c@test.com",
			"missing domain part":                      "abc@",
			"domain starting with number":              "abc@4test.co",
			"short domain host":                        "abc@h.com",
			"special character in domain part":         "abc@te$t.com",
			"short top level domain":                   "abc@test.c",
			"number in top level domain":               "abc@test.c1",
			"special character in top level domain":    "abc@test.c#m",
		}

		emailID := new(types.EmailID)

		for title, emailString := range testCases {
			suite.Run(title, func() {
				err := emailID.UnmarshalText(emailString)

				suite.NotNil(err)
				suite.Equal(fmt.Sprintf("can not unmarshal %s as an email id", emailString), err.Error())
				suite.Equal(new(types.EmailID), emailID)
			})
		}
	})
}

func (suite *EmailIDTestSuite) TestEmailID_Validate() {
	suite.Run("return error for invalid email values", func() {
		testCases := map[string]string{
			"empty value":                  "",
			"value with just white spaces": " \t    ",
		}

		for title, testCase := range testCases {
			suite.Run(title, func() {
				emailID := new(types.EmailID)
				_ = emailID.UnmarshalText(testCase)

				err := emailID.Validate()

				suite.Require().NotNil(err)
				suite.Equal("email id value can not be empty", err.Error())
			})
		}
	})

	suite.Run("return no error for valid values", func() {
		emailID := new(types.EmailID)
		err := emailID.UnmarshalText("abc@test.com")
		suite.Require().Nil(err)

		err = emailID.Validate()

		suite.Nil(err)
	})
}
