package types_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"

	"gitlab.com/abhishp/rain-csv-parser/pkg/domain/types"

	"gitlab.com/abhishp/rain-csv-parser/configs"
)

type MoneyTestSuite struct {
	suite.Suite
}

func TestMoney(t *testing.T) {
	suite.Run(t, new(MoneyTestSuite))
}

func (suite *MoneyTestSuite) SetupSuite() {
	_ = configs.Init("../../test/.fixtures/configs/test.configs.yml")
}

func (suite *MoneyTestSuite) TestMoney_UnmarshalText() {
	suite.Run("returns error", func() {
		testCases := map[string]string{
			"alphabetic values":                     "two",
			"starting with two negatives":           "--1",
			"multiple signs with space":             "- -1",
			"invalid code for currency":             "ID 1",
			"invalid separator distance":            "11,23",
			"space between separated numbers":       "11, 23",
			"sign in middle of number":              "1-123",
			"invalid number of digits post decimal": ".1234",
			"just decimal":                          ".",
			"ending with decimal":                   "1.",
			"symbol with multiple signs":            "- $ -1",
			"code with multiple signs":              "- USD -1",
			"double currency symbol":                "$$ 1",
		}

		for title, value := range testCases {
			suite.Run(title, func() {
				money := new(types.Money)

				err := money.UnmarshalText(value)

				suite.Require().NotNil(err)
				suite.Equal(fmt.Sprintf("can not unmarshal %s as money", value), err.Error())
				suite.Empty(money.String())
			})
		}

		invalidSymbols := "`~!@#%^&*()[]{}_=+;:'\"<>,/?|\""
		for _, symbol := range invalidSymbols {
			suite.Run(fmt.Sprintf("invalid symbol %c  for currency", symbol), func() {
				money := new(types.Money)

				err := money.UnmarshalText(fmt.Sprintf("%c 1", symbol))

				suite.Require().NotNil(err)
				suite.Equal(fmt.Sprintf("can not unmarshal %c 1 as money", symbol), err.Error())
				suite.Empty(money.String())
			})
		}
	})

	suite.Run("return no error and updates value", func() {
		testCases := map[string]struct {
			value    string
			expected string
		}{
			"empty value":                                {value: ""},
			"value with all white spaces":                {value: "   \t   "},
			"numeric value":                              {value: "1", expected: "1"},
			"value with decimal":                         {value: "1.23", expected: "1.23"},
			"value with separator":                       {value: "1,234.5", expected: "1234.5"},
			"value with million style separator":         {value: "2,021,234.5", expected: "2021234.5"},
			"value with lac style separator":             {value: "20,21,234.6", expected: "2021234.6"},
			"value with negative sign":                   {value: "-1.56", expected: "-1.56"},
			"value with negative sign and symbol":        {value: "-₹3.56", expected: "₹ -3.56"},
			"value with sign and space before symbol":    {value: " -$1.0", expected: "$ -1.0"},
			"value with sign and space after symbol":     {value: "-₭ 1.56", expected: "₭ -1.56"},
			"with separator":                             {value: "- £ 1,234.56", expected: "£ -1234.56"},
			"without separator":                          {value: "-\t\t₿\t234.567", expected: "₿ -234.567"},
			"with code":                                  {value: "INR 34.56", expected: "INR 34.56"},
			"with code and sign":                         {value: "-AUD 456.78", expected: "AUD -456.78"},
			"with code, sign and separator":              {value: "- Nok 5,678.9", expected: "Nok -5678.9"},
			"start with code, then sign and separator":   {value: "inr - 2,34,567.8", expected: "inr -234567.8"},
			"start with symbol, then sign and separator": {value: "€ - 12,345,679.8", expected: "€ -12345679.8"},
		}

		for title, testCase := range testCases {
			suite.Run(title, func() {
				money := new(types.Money)

				err := money.UnmarshalText(testCase.value)

				suite.Require().Nil(err)
				suite.Equal(testCase.expected, money.String())
			})
		}
	})
}

func (suite *MoneyTestSuite) TestMoney_Validate() {
	suite.Run("should return error", func() {
		testCases := map[string]struct {
			value            string
			expectedErrorMsg string
		}{
			"empty string":           {value: "", expectedErrorMsg: "money value can not be empty"},
			"whitespace only string": {value: "\t\r   \t\t", expectedErrorMsg: "money value can not be empty"},
			"negative amounts without currency code or symbol": {value: "-1", expectedErrorMsg: "negative amounts not allowed"},
			"negative amounts with currency code":              {value: "-USD1", expectedErrorMsg: "negative amounts not allowed"},
			"negative amounts with currency symbol":            {value: "-$1", expectedErrorMsg: "negative amounts not allowed"},
		}

		for title, testCase := range testCases {
			suite.Run(title, func() {
				money := new(types.Money)
				err := money.UnmarshalText(testCase.value)
				suite.Require().Nil(err)

				err = money.Validate()

				suite.Require().NotNil(err)
				suite.Equal(testCase.expectedErrorMsg, err.Error())
			})
		}
	})
}

func (suite *MoneyTestSuite) TestMoney_String() {
	suite.Run("should format the money with currency and sign", func() {
		testCases := map[string]string{
			"$1":          "$ 1",
			"$ 1,111":     "$ 1111",
			"\t  $ 1,345": "$ 1345",
			"$ -1,234":    "$ -1234",
			"-$ 2345":     "$ -2345",
			"- $ 2345":    "$ -2345",
		}

		for value, expectedStringValue := range testCases {
			suite.Run(fmt.Sprintf("when value is %s", value), func() {
				money := new(types.Money)
				err := money.UnmarshalText(value)
				suite.Require().Nil(err)

				suite.Equal(expectedStringValue, money.String())
			})
		}
	})
}
