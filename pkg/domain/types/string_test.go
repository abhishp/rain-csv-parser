package types_test

import (
	"testing"

	"github.com/stretchr/testify/suite"

	"gitlab.com/abhishp/rain-csv-parser/pkg/domain/types"
)

type StringTestSuite struct {
	suite.Suite
}

func TestString(t *testing.T) {
	suite.Run(t, new(StringTestSuite))
}

func (suite *StringTestSuite) TestString_UnmarshalText() {
	suite.Run("should update the value after trimming spaces", func() {
		testCases := map[string]struct {
			value    string
			expected string
		}{
			"empty string":                                  {},
			"value with whitespace in prefix":               {value: "   \t    \r Value", expected: "Value"},
			"value with whitespace in suffix":               {value: "Value   \t    \n ", expected: "Value"},
			"value with whitespace in prefix & suffix both": {value: "   \t    \r Value   \r    \n ", expected: "Value"},
		}
		for title, testCase := range testCases {
			suite.Run(title, func() {
				s := new(types.String)

				err := s.UnmarshalText(testCase.value)

				suite.Require().Nil(err)
				suite.Equal(testCase.expected, s.String())
			})
		}
	})
}

func (suite *StringTestSuite) TestString_Validate() {
	suite.Run("should return error", func() {
		testCases := map[string]*types.String{
			"nil value":        nil,
			"empty string":     suite.buildStringValue(""),
			"whitespaces only": suite.buildStringValue("\t \r  \r   \n"),
		}

		for title, str := range testCases {
			suite.Run(title, func() {
				err := str.Validate()

				suite.Require().NotNil(err)
				suite.Equal("string value can not be empty", err.Error())
			})
		}
	})

	suite.Run("should not return error when ", func() {
		str := suite.buildStringValue("\t  some value \r")

		err := str.Validate()

		suite.Nil(err)
	})
}

func (suite *StringTestSuite) buildStringValue(str string) *types.String {
	s := types.String{}
	_ = s.UnmarshalText(str)
	return &s
}
