package types

import (
	"fmt"
	"regexp"
	"strings"

	"github.com/pkg/errors"

	"gitlab.com/abhishp/rain-csv-parser/pkg/domain/constants"
)

func init() {
	registerInitFunc(constants.AttributeTypeMoney, func() Value {
		return new(Money)
	})
}

var (
	currencyRegex        = `(?:[[:alpha:]]{3,4}|[\p{Lu}]{0,2}[\p{Sc}][\p{Lu}]?)`
	signRegex            = `\-`
	currencyAndSignRegex = fmt.Sprintf(`(?:(?P<signStart>%[1]s)\s*(?P<currencyMid>%[2]s)?|(?P<currencyStart>%[2]s)\s*(?P<signMid>%[1]s)?)?`, signRegex, currencyRegex)
	amountRegex          = `(?P<amount>(?:[1-9]\d{0,2}(?:(?:,\d{2,3})*(?:,\d{3})|\d*)|0)(?:\.\d{1,3})?|\.\d{1,3})`

	moneyRegex = regexp.MustCompile(fmt.Sprintf(`^%s\s*%s$`, currencyAndSignRegex, amountRegex))

	space = " "
)

type Money struct {
	amount   string
	currency *string
	sign     *string
}

func (m *Money) UnmarshalText(value string) error {
	value = strings.TrimSpace(value)
	switch {
	case value == "":
		return nil
	case !moneyRegex.MatchString(value):
		return errors.Errorf("can not unmarshal %s as money", value)
	}
	money := strings.SplitN(moneyRegex.ReplaceAllString(value, "$signStart$signMid;$currencyStart$currencyMid;$amount"), ";", 3)
	if money[0] != "" {
		m.sign = &money[0]
	}
	if money[1] != "" {
		m.currency = &money[1]
	}
	m.amount = strings.ReplaceAll(money[2], ",", "")
	return nil
}

func (m *Money) String() string {
	if m.amount == "" {
		return ""
	}
	moneyBuilder := strings.Builder{}
	if m.currency != nil {
		moneyBuilder.WriteString(*m.currency)
		moneyBuilder.WriteString(space)
	}
	if m.sign != nil {
		moneyBuilder.WriteString(*m.sign)
	}
	moneyBuilder.WriteString(m.amount)
	return moneyBuilder.String()
}

func (m *Money) Validate() error {
	if m.amount == "" {
		return errors.New("money value can not be empty")
	}

	if m.sign != nil && *m.sign == "-" {
		return errors.New("negative amounts not allowed")
	}
	return nil
}
