package types

import (
	"regexp"
	"strings"

	"github.com/pkg/errors"

	"gitlab.com/abhishp/rain-csv-parser/pkg/domain/constants"
)

func init() {
	registerInitFunc(constants.AttributeTypeEmailID, func() Value {
		return new(EmailID)
	})
}

var emailIDRegex = regexp.MustCompile(`^[a-zA-Z0-9][\w.]*@[a-z][a-z0-9-]{0,62}[a-z0-9]\.[a-zA-Z]{2,4}$`)

type EmailID struct {
	value string
}

func (e *EmailID) UnmarshalText(value string) error {
	value = strings.TrimSpace(value)
	switch {
	case value == "":
		return nil
	case !emailIDRegex.MatchString(value):
		return errors.Errorf("can not unmarshal %s as an email id", value)
	}

	e.value = value
	return nil
}

func (e EmailID) String() string {
	return e.value
}

func (e *EmailID) Validate() error {
	if e == nil || e.value == "" {
		return errors.New("email id value can not be empty")
	}

	return nil
}
