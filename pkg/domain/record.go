package domain

import "gitlab.com/abhishp/rain-csv-parser/pkg/domain/types"

type Record map[*Attribute]types.Value
