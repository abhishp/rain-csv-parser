package domain_test

import (
	"encoding/json"
	"errors"
	"testing"

	"github.com/stretchr/testify/suite"

	"gitlab.com/abhishp/rain-csv-parser/pkg/domain"
	"gitlab.com/abhishp/rain-csv-parser/pkg/domain/constraints"
	"gitlab.com/abhishp/rain-csv-parser/test/mocks"
)

type CSVAttributesTestSuite struct {
	suite.Suite
}

func TestCSVAttributes(t *testing.T) {
	suite.Run(t, new(CSVAttributesTestSuite))
}

func (suite *CSVAttributesTestSuite) TestBuildAttributes() {
	suite.Run("return error", func() {
		testCases := map[string]json.RawMessage{
			"empty json":   json.RawMessage(""),
			"invalid json": json.RawMessage("["),
			"extra fields": json.RawMessage(`[{"extra": "value"}]`),
		}
		for title, attributesConfig := range testCases {
			suite.Run(title, func() {
				attributes, err := domain.BuildAttributes(attributesConfig)

				suite.Nil(attributes)
				suite.NotNil(err)
				suite.Contains(err.Error(), "invalid attributes config: ")
			})
		}
	})

	suite.Run("return attributes with constraints", func() {
		attributesConfig := json.RawMessage(`[{"name":"name","outputHeader":"Name","inputHeaders":["First Name","Last Name"],"type":"string"},{"name":"emailId","outputHeader":"Email ID","inputHeaders":["Email"],"type":"email","isUnique":true}]`)
		expectedAttributes := domain.Attributes{
			{Name: "name", OutputHeader: "Name", InputHeaders: []string{"First Name", "Last Name"}, Type: "string"},
			{Name: "emailId", OutputHeader: "Email ID", InputHeaders: []string{"Email"}, Type: "email", IsUnique: true},
		}
		expectedAttributes[1].AddConstraint(constraints.Uniqueness())

		attributes, err := domain.BuildAttributes(attributesConfig)

		suite.Nil(err)
		suite.Equal(expectedAttributes, attributes)
	})
}

func (suite *CSVAttributesTestSuite) TestAttribute_ValidateValue() {
	suite.Run("constraint is not setup", func() {
		suite.Run("does nothing", func() {
			value := &mocks.Value{}
			attribute := domain.Attribute{}

			suite.Nil(attribute.ValidateValue(value))
			value.AssertExpectations(suite.T())
		})
	})

	suite.Run("constraint is setup", func() {
		attribute := domain.Attribute{}
		constraint := &mocks.Constraint{}
		attribute.AddConstraint(constraint)

		suite.Run("should validate the setup constraint", func() {
			value := &mocks.Value{}
			constraint.ExpectedCalls = nil

			constraint.On("Validate", value).Once().Return(nil)

			suite.Nil(attribute.ValidateValue(value))
			constraint.AssertExpectations(suite.T())
			value.AssertExpectations(suite.T())
		})

		suite.Run("should return error when the setup constraint validation fails", func() {
			value := &mocks.Value{}
			constraint.ExpectedCalls = nil
			expectedError := errors.New("validation failure from constraint")

			constraint.On("Validate", value).Once().Return(expectedError)

			suite.Equal(expectedError, attribute.ValidateValue(value))
			constraint.AssertExpectations(suite.T())
			value.AssertExpectations(suite.T())
		})
	})
}
