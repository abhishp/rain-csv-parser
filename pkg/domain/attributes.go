package domain

import (
	"bytes"
	"encoding/json"

	"github.com/pkg/errors"

	"gitlab.com/abhishp/rain-csv-parser/pkg/domain/constraints"
	"gitlab.com/abhishp/rain-csv-parser/pkg/domain/types"
)

type Attribute struct {
	Name         string   `json:"name"`
	InputHeaders []string `json:"inputHeaders"`
	OutputHeader string   `json:"outputHeader"`
	Type         string   `json:"type"`
	IsUnique     bool     `json:"isUnique"`
	constraint   constraints.Constraint
}

type Attributes []*Attribute

func BuildAttributes(attributeConfig json.RawMessage) (attributes Attributes, err error) {
	decoder := json.NewDecoder(bytes.NewReader(attributeConfig))
	decoder.DisallowUnknownFields()
	if err = decoder.Decode(&attributes); err != nil {
		return nil, errors.Wrap(err, "invalid attributes config")
	}
	for _, attribute := range attributes {
		if attribute.IsUnique {
			attribute.AddConstraint(constraints.Uniqueness())
		}
	}
	return
}

func (a *Attribute) AddConstraint(constraint constraints.Constraint) {
	a.constraint = constraint
}

func (a *Attribute) ValidateValue(value types.Value) error {
	if a.constraint == nil {
		return nil
	}
	return a.constraint.Validate(value)
}
