package constraints_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"

	"gitlab.com/abhishp/rain-csv-parser/pkg/domain/constraints"
	"gitlab.com/abhishp/rain-csv-parser/test/mocks"
)

type UniquenessTestSuite struct {
	suite.Suite
}

func TestUniqueness(t *testing.T) {
	suite.Run(t, new(UniquenessTestSuite))
}

func (suite *UniquenessTestSuite) TestUniqueness_Validate() {
	suite.Run("should return nil when value does not exist", func() {
		uc := constraints.Uniqueness()
		for idx := 0; idx < 10; idx++ {
			value := &mocks.Value{}
			valueString := fmt.Sprintf("value %d", idx+1)
			value.On("String").Return(valueString)
			suite.Nil(uc.Validate(value), "failed for value %s", valueString)
		}
	})

	suite.Run("should return error when value already exists", func() {
		uc := constraints.Uniqueness()
		for idx := 0; idx < 10; idx++ {
			value := &mocks.Value{}
			valueString := fmt.Sprintf("value %d", idx+1)
			value.On("String").Return(valueString)
			suite.Require().Nil(uc.Validate(value), "failed for value %s", valueString)
		}

		value := &mocks.Value{}
		value.On("String").Return("value 5")
		err := uc.Validate(value)

		suite.NotNil(err)
		suite.Equal("value 5: uniqueness constraint violated", err.Error())
	})
}
