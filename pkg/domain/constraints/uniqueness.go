package constraints

import (
	"sync"

	"github.com/pkg/errors"

	"gitlab.com/abhishp/rain-csv-parser/pkg/domain/types"
)

type uniqueness struct {
	values map[string]struct{}
	mutex  sync.Mutex
}

func Uniqueness() Constraint {
	return &uniqueness{values: map[string]struct{}{}}
}

func (u *uniqueness) Validate(value types.Value) error {
	valueString := value.String()
	if _, isPresent := u.values[valueString]; isPresent {
		return errors.Errorf("%s: uniqueness constraint violated", valueString)
	}
	u.mutex.Lock()
	defer u.mutex.Unlock()
	u.values[valueString] = struct{}{}
	return nil
}
