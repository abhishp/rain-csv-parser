package constraints

import "gitlab.com/abhishp/rain-csv-parser/pkg/domain/types"

type Constraint interface {
	Validate(value types.Value) error
}
