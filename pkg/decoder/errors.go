package decoder

import (
	"fmt"
	"strings"
)

const errSeparator = ", "

type Error struct {
	message string
	reasons []string
}

func NewError(csvRow []string) *Error {
	return &Error{message: fmt.Sprintf(`can not decode "%s"`, strings.Join(csvRow, errSeparator))}
}

func (e *Error) Error() string {
	return e.message + ": " + strings.Join(e.reasons, errSeparator)
}

func (e *Error) AddReasons(err ...error) {
	for _, er := range err {
		e.reasons = append(e.reasons, er.Error())
	}
}
