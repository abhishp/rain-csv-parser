package decoder_test

import (
	"testing"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/suite"

	"gitlab.com/abhishp/rain-csv-parser/configs"
	"gitlab.com/abhishp/rain-csv-parser/pkg/decoder"
	"gitlab.com/abhishp/rain-csv-parser/pkg/domain"
	"gitlab.com/abhishp/rain-csv-parser/pkg/domain/constants"
)

type DecoderTestSuite struct {
	suite.Suite
	attributes domain.Attributes
	csvHeader  []string
}

func TestParser(t *testing.T) {
	suite.Run(t, new(DecoderTestSuite))
}

func (suite *DecoderTestSuite) SetupSuite() {
	_ = configs.Init("../../test/.fixtures/configs/test.configs.yml")
	suite.attributes = domain.Attributes{
		{Name: "f1", InputHeaders: []string{"Field 1"}, Type: constants.AttributeTypeString},
		{Name: "f2", InputHeaders: []string{"Field 2"}, Type: constants.AttributeTypeString},
		{Name: "f3", InputHeaders: []string{"Field 3 - 1", "Field 3 - 2"}, Type: constants.AttributeTypeString},
	}
	suite.csvHeader = []string{"Field 2", "Field 3 - 2", "Field 1", "Field 3 - 1", "Field 4"}
}

func (suite *DecoderTestSuite) TestNewParser() {
	suite.Run("returns error when one or more attributes dont have any fields in the csv", func() {
		reader, err := decoder.NewDecoder(suite.attributes, []string{"Field 1", "Field 2", "Field 3"})

		suite.Nil(reader)
		suite.NotNil(err)
		suite.Equal("csv does not have all the required fields: missing headers Field 3 - 1,Field 3 - 2", err.Error())
	})

	suite.Run("returns no error all the attributes have fields", func() {
		reader, err := decoder.NewDecoder(suite.attributes, suite.csvHeader)

		suite.Nil(err)
		suite.Implements((*decoder.Decoder)(nil), reader)
	})
}

func (suite *DecoderTestSuite) TestDecode() {
	suite.Run("return record", func() {
		testCases := map[string]struct {
			csvRecord      []string
			expectedRecord []string
		}{
			"return empty values for empty csv row": {
				csvRecord:      []string{"", "", "", "", ""},
				expectedRecord: []string{"", "", ""},
			},
			"map values to correct attributes": {
				csvRecord:      []string{"v2", "", "v1", "v31", "v4"},
				expectedRecord: []string{"v1", "v2", "v31"},
			},
			"return empty value for multi field attributes": {
				csvRecord:      []string{"v2", "", "v1", "", "v4"},
				expectedRecord: []string{"v1", "v2", ""},
			},
			"map first non empty value for multi field attributes": {
				csvRecord:      []string{"v2", "v32", "v1", "", "v4"},
				expectedRecord: []string{"v1", "v2", "v32"},
			},
			"concatenate value for multi field attributes having both field values present in order": {
				csvRecord:      []string{"v2", "v32", "v1", "v31", "v4"},
				expectedRecord: []string{"v1", "v2", "v31 v32"},
			},
		}
		d, _ := decoder.NewDecoder(suite.attributes, suite.csvHeader)

		for title, testCase := range testCases {
			suite.Run(title, func() {
				record, err := d.Decode(testCase.csvRecord)

				suite.Require().Nil(err)
				for idx, attribute := range suite.attributes {
					suite.Equal(testCase.expectedRecord[idx], record[attribute].String())
				}
			})
		}
	})

	suite.Run("types", func() {
		attributes := domain.Attributes{
			{Name: "f1", InputHeaders: []string{"f1"}, Type: constants.AttributeTypeString},
			{Name: "f2", InputHeaders: []string{"f2"}, Type: constants.AttributeTypeEmailID},
			{Name: "f3", InputHeaders: []string{"f3"}, Type: constants.AttributeTypeMoney},
		}
		headers := []string{"f1", "f2", "f3"}
		d, _ := decoder.NewDecoder(attributes, headers)
		suite.Run("return error", func() {
			testCases := map[string]struct {
				csvRow          []string
				expectedReasons []string
			}{
				"when email field is invalid": {
					csvRow:          []string{"v1", "v2", "1"},
					expectedReasons: []string{"can not unmarshal v2 as an email id"},
				},
				"when money field is invalid": {
					csvRow:          []string{"v1", "abc@test.com", "1."},
					expectedReasons: []string{"can not unmarshal 1. as money"},
				},
				"when email & money field are invalid": {
					csvRow:          []string{"v1", "abc", "1."},
					expectedReasons: []string{"can not unmarshal abc as an email id", "can not unmarshal 1. as money"},
				},
			}

			for title, testCase := range testCases {
				suite.Run(title, func() {
					expectedError := suite.buildExpectedError(testCase.csvRow, testCase.expectedReasons)
					record, err := d.Decode(testCase.csvRow)

					suite.Nil(record)
					suite.Require().NotNil(err)
					suite.Equal(expectedError, err)
				})
			}
		})

		suite.Run("return record with correct typed values without error", func() {
			stringValue := "v1"
			emailValue := "abc@test.com"
			moneyValue := "INR 1234.56"

			record, err := d.Decode([]string{stringValue, emailValue, moneyValue})

			suite.Require().Nil(err)
			suite.Len(record, 3)
			suite.Equal(stringValue, record[attributes[0]].String())
			suite.Equal(emailValue, record[attributes[1]].String())
			suite.Equal(moneyValue, record[attributes[2]].String())
		})
	})
}

func (suite *DecoderTestSuite) buildExpectedError(record []string, reasons []string) *decoder.Error {
	err := decoder.NewError(record)
	for _, reason := range reasons {
		err.AddReasons(errors.New(reason))
	}
	return err
}
