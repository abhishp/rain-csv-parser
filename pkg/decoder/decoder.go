package decoder

import (
	"strings"

	"github.com/pkg/errors"

	"gitlab.com/abhishp/rain-csv-parser/pkg/domain"
	"gitlab.com/abhishp/rain-csv-parser/pkg/domain/types"
)

var space = " "

type Decoder interface {
	Decode(record []string) (domain.Record, error)
}

type decoder struct {
	attributes       domain.Attributes
	attributeMapping map[*domain.Attribute][]int
}

func NewDecoder(attributes domain.Attributes, csvHeader []string) (Decoder, error) {
	attributeMapping, missingHeaders := generateAttributeMapping(attributes, csvHeader)
	if len(missingHeaders) > 0 {
		return nil, errors.Errorf("csv does not have all the required fields: missing headers %s", strings.Join(missingHeaders, ","))
	}
	return &decoder{attributes: attributes, attributeMapping: attributeMapping}, nil
}

func (d *decoder) Decode(record []string) (domain.Record, error) {
	r := make(domain.Record, len(d.attributes))
	var errs []error
	for _, attribute := range d.attributes {
		value, err := decode(readAttribute(record, d.attributeMapping[attribute]), attribute.Type)
		if err != nil {
			errs = append(errs, err)
		}
		r[attribute] = value
	}
	if len(errs) != 0 {
		err := NewError(record)
		err.AddReasons(errs...)
		return nil, err
	}
	return r, nil
}

func readAttribute(record []string, columnIndices []int) string {
	if len(columnIndices) == 1 {
		return record[columnIndices[0]]
	}

	valueBuilder := strings.Builder{}
	for _, idx := range columnIndices {
		if fieldVal := record[idx]; fieldVal != "" {
			valueBuilder.WriteString(space)
			valueBuilder.WriteString(fieldVal)
		}
	}
	if valueBuilder.Len() > 0 {
		return valueBuilder.String()[1:]
	}
	return ""
}

func generateAttributeMapping(attributes domain.Attributes, headerRow []string) (map[*domain.Attribute][]int, []string) {
	attributeMapping := make(map[*domain.Attribute][]int, len(attributes))
	var missingHeaders []string
	for _, attribute := range attributes {
		for _, header := range attribute.InputHeaders {
			if idx := findHeaderIndex(headerRow, header); idx >= 0 {
				attributeMapping[attribute] = append(attributeMapping[attribute], idx)
			}
		}
		if _, isPresent := attributeMapping[attribute]; !isPresent {
			missingHeaders = append(missingHeaders, attribute.InputHeaders...)
		}
	}
	return attributeMapping, missingHeaders
}

func findHeaderIndex(csvHeaders []string, header string) int {
	for idx, csvHeader := range csvHeaders {
		if strings.EqualFold(csvHeader, header) {
			return idx
		}
	}
	return -1
}

func decode(value, valueType string) (types.Value, error) {
	val := types.ValueFor(valueType)
	if err := val.UnmarshalText(value); err != nil {
		return nil, err
	}
	return val, nil
}
