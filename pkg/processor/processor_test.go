package processor_test

import (
	"io"
	"os"
	"testing"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"

	"gitlab.com/abhishp/rain-csv-parser/pkg/csv"
	"gitlab.com/abhishp/rain-csv-parser/pkg/domain"
	"gitlab.com/abhishp/rain-csv-parser/pkg/processor"
	"gitlab.com/abhishp/rain-csv-parser/test/mocks"
)

type ProcessorTestSuite struct {
	suite.Suite
	mockReader  *mocks.Reader
	mockDecoder *mocks.Decoder
}

func TestProcessor(t *testing.T) {
	suite.Run(t, new(ProcessorTestSuite))
}

func (suite *ProcessorTestSuite) SetupTest() {
	suite.mockDecoder = &mocks.Decoder{}
	suite.mockReader = &mocks.Reader{}
	log.Logger = log.Logger.Output(io.Discard)
}

func (suite *ProcessorTestSuite) TearDownTest() {
	log.Logger = log.Logger.Output(os.Stderr)
}

func (suite *ProcessorTestSuite) TestNewProcessor() {
	suite.Run("should return new processor", func() {
		proc := processor.New(suite.mockReader, suite.mockDecoder, nil, nil)

		suite.Require().NotNil(proc)
		suite.Implements((*processor.Processor)(nil), proc)
	})
}

func (suite *ProcessorTestSuite) TestProcess() {
	suite.Run("should report failures on error callback", func() {
		attributes := domain.Attributes{{Name: "attr1", OutputHeader: "attr1"}, {Name: "attr2", OutputHeader: "attr2"}}
		constraint := &mocks.Constraint{}
		attributes[1].AddConstraint(constraint)

		testCases := map[string]func() *mocks.ProcessorCallback{
			"when read call fails for csv reader": func() *mocks.ProcessorCallback {
				expectedError := errors.New("csv reader error")
				callbackMocks := &mocks.ProcessorCallback{}

				suite.mockReader.On("Read").Once().Return(nil, expectedError)
				callbackMocks.On("ErrorCallback", ([]string)(nil), int64(1), expectedError)

				return callbackMocks
			},
			"when decoder fails to decode the csv record": func() *mocks.ProcessorCallback {
				csvRecord := []string{"csv", "record"}
				callbackMocks := &mocks.ProcessorCallback{}

				suite.mockReader.On("Read").Once().Return(csvRecord, nil)
				expectedError := errors.New("decoder error")
				suite.mockDecoder.On("Decode", csvRecord).Once().
					Return(nil, expectedError)
				callbackMocks.On("ErrorCallback", csvRecord, int64(1), expectedError)

				return callbackMocks
			},
			"when validation fails for parsed record": func() *mocks.ProcessorCallback {
				csvRecord := []string{"csv", "record"}
				mockedValueOne := &mocks.Value{}
				mockedValueTwo := &mocks.Value{}
				record := domain.Record{
					attributes[0]: mockedValueOne,
					attributes[1]: mockedValueTwo,
				}
				valueTwoError := errors.New("error for attr2")
				callbackMocks := &mocks.ProcessorCallback{}

				suite.mockReader.On("Read").Once().Return(csvRecord, nil)
				suite.mockDecoder.On("Decode", csvRecord).Once().Return(record, nil)
				mockedValueOne.On("Validate").Return(nil)
				mockedValueTwo.On("Validate").Return(valueTwoError)
				callbackMocks.On("ErrorCallback", csvRecord, int64(1), mock.MatchedBy(func(err error) bool {
					return suite.Equal("validation error: attr2: error for attr2", err.Error())
				}))

				return callbackMocks
			},
			"when attribute constraint validation fails for parsed record": func() *mocks.ProcessorCallback {
				csvRecord := []string{"csv", "record"}
				mockedValueOne := &mocks.Value{}
				mockedValueTwo := &mocks.Value{}
				record := domain.Record{
					attributes[0]: mockedValueOne,
					attributes[1]: mockedValueTwo,
				}
				constraintError := errors.New("constraint error")
				callbackMocks := &mocks.ProcessorCallback{}

				suite.mockReader.On("Read").Once().Return(csvRecord, nil)
				suite.mockDecoder.On("Decode", csvRecord).Once().Return(record, nil)
				mockedValueOne.On("Validate").Return(nil)
				mockedValueTwo.On("Validate").Return(nil)
				constraint.On("Validate", mockedValueTwo).Return(constraintError)
				callbackMocks.On("ErrorCallback", csvRecord, int64(1), mock.MatchedBy(func(err error) bool {
					return suite.Equal("validation error: attr2: constraint error", err.Error())
				}))

				return callbackMocks
			},
			"when validation fails for multiple values in the parsed record": func() *mocks.ProcessorCallback {
				csvRecord := []string{"csv", "record"}
				mockedValueOne := &mocks.Value{}
				mockedValueTwo := &mocks.Value{}
				record := domain.Record{
					attributes[0]: mockedValueOne,
					attributes[1]: mockedValueTwo,
				}
				valueOneError := errors.New("error for attr1")
				valueTwoError := errors.New("error for attr2")

				callbackMocks := &mocks.ProcessorCallback{}

				suite.mockReader.On("Read").Once().Return(csvRecord, nil)
				suite.mockDecoder.On("Decode", csvRecord).Once().Return(record, nil)
				mockedValueOne.On("Validate").Return(valueOneError)
				mockedValueTwo.On("Validate").Return(valueTwoError)
				callbackMocks.On("ErrorCallback", csvRecord, int64(1), mock.MatchedBy(func(err error) bool {
					return suite.Contains(err.Error(), "attr1: error for attr1") &&
						suite.Contains(err.Error(), "attr2: error for attr2") &&
						suite.Contains(err.Error(), "validation error: ")
				}))

				return callbackMocks
			},
		}

		for title, testCase := range testCases {
			suite.Run(title, func() {
				suite.SetupTest()
				constraint.ExpectedCalls = nil
				callbackMocks := testCase()
				suite.mockReader.On("Read").Once().Return(nil, csv.EOF)
				proc := suite.newProcessor(callbackMocks)

				metadata := processor.NewMetadata()
				proc.Process(metadata)

				suite.mockReader.AssertExpectations(suite.T())
				suite.mockDecoder.AssertExpectations(suite.T())
				callbackMocks.AssertNotCalled(suite.T(), "SuccessCallback")
				callbackMocks.AssertExpectations(suite.T())
				suite.Equal(int64(1), metadata.Total())
				suite.Equal(int64(1), metadata.FailureCount())
				suite.Zero(metadata.SuccessCount())
			})
		}
	})

	suite.Run("should report success on success callback", func() {
		suite.SetupTest()
		callbackMocks := &mocks.ProcessorCallback{}
		csvRecords := [][]string{{"csv1", "record1"}, {"csv2", "record2"}}
		attributes := domain.Attributes{{Name: "attr1"}, {Name: "attr2"}}
		records := [2]domain.Record{{}, {}}
		for _, attribute := range attributes {
			mockValueOne := &mocks.Value{}
			mockValueTwo := &mocks.Value{}
			records[0][attribute] = mockValueOne
			records[1][attribute] = mockValueTwo
			mockValueOne.On("Validate").Return(nil)
			mockValueTwo.On("Validate").Return(nil)
		}
		suite.mockReader.On("Read").Once().Return(csvRecords[0], nil)
		suite.mockReader.On("Read").Once().Return(csvRecords[1], nil)
		suite.mockReader.On("Read").Once().Return(nil, csv.EOF)
		suite.mockDecoder.On("Decode", csvRecords[0]).Once().Return(records[0], nil)
		suite.mockDecoder.On("Decode", csvRecords[1]).Once().Return(records[1], nil)
		callbackMocks.On("SuccessCallback", records[0], int64(1))
		callbackMocks.On("SuccessCallback", records[1], int64(2))
		proc := suite.newProcessor(callbackMocks)

		metadata := processor.NewMetadata()
		proc.Process(metadata)

		suite.mockReader.AssertExpectations(suite.T())
		suite.mockDecoder.AssertExpectations(suite.T())
		callbackMocks.AssertNotCalled(suite.T(), "ErrorCallback")
		callbackMocks.AssertExpectations(suite.T())
		suite.Equal(int64(2), metadata.Total())
		suite.Equal(int64(2), metadata.SuccessCount())
		suite.Zero(metadata.FailureCount())
	})

	suite.Run("should report success and error on callbacks", func() {
		suite.SetupTest()
		callbackMocks := &mocks.ProcessorCallback{}
		csvRecords := [3][]string{{"csv1", "record1"}, {"csv2", "record2"}, {"csv3", "record3"}}
		attributes := domain.Attributes{{Name: "attr1"}, {Name: "attr2", OutputHeader: "attr2"}, {Name: "attr3"}}
		records := [3]domain.Record{{}, {}, {}}
		expectedError := errors.New("error for attr2 for record 2")
		for _, attribute := range attributes {
			for _, record := range records {
				mockValue := &mocks.Value{}
				record[attribute] = mockValue
				mockValue.On("Validate").Return(nil)
			}
		}
		errorValue := &mocks.Value{}
		records[1][attributes[1]] = errorValue

		errorValue.On("Validate").Return(expectedError)
		suite.mockReader.On("Read").Once().Return(csvRecords[0], nil)
		suite.mockReader.On("Read").Once().Return(csvRecords[1], nil)
		suite.mockReader.On("Read").Once().Return(csvRecords[2], nil)
		suite.mockReader.On("Read").Once().Return(nil, csv.EOF)
		suite.mockDecoder.On("Decode", csvRecords[0]).Once().Return(records[0], nil)
		suite.mockDecoder.On("Decode", csvRecords[1]).Once().Return(records[1], nil)
		suite.mockDecoder.On("Decode", csvRecords[2]).Once().Return(records[2], nil)
		callbackMocks.On("SuccessCallback", records[0], int64(1))
		callbackMocks.On("SuccessCallback", records[2], int64(3))
		callbackMocks.On("ErrorCallback", csvRecords[1], int64(2), mock.MatchedBy(func(err error) bool {
			return suite.Equal("validation error: attr2: error for attr2 for record 2", err.Error())
		}))
		proc := suite.newProcessor(callbackMocks)

		metadata := processor.NewMetadata()
		proc.Process(metadata)

		suite.mockReader.AssertExpectations(suite.T())
		suite.mockDecoder.AssertExpectations(suite.T())
		callbackMocks.AssertExpectations(suite.T())
		suite.Equal(int64(3), metadata.Total())
		suite.Equal(int64(1), metadata.FailureCount())
		suite.Equal(int64(2), metadata.SuccessCount())
	})
}

func (suite *ProcessorTestSuite) newProcessor(callbackMocks *mocks.ProcessorCallback) processor.Processor {
	return processor.New(
		suite.mockReader,
		suite.mockDecoder,
		callbackMocks.SuccessCallback,
		callbackMocks.ErrorCallback,
	)
}
