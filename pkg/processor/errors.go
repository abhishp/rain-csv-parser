package processor

import "strings"

type multiCauseError struct {
	msg   string
	cause []error
}

func NewMultiCauseError(message string, err ...error) error {
	if len(err) == 0 {
		return nil
	}
	return &multiCauseError{
		msg:   message,
		cause: err,
	}
}

func (mce *multiCauseError) Error() string {
	errBuilder := &strings.Builder{}
	errBuilder.WriteString(mce.msg)
	errBuilder.WriteString(": ")
	errBuilder.WriteString(mce.cause[0].Error())
	for idx := 1; idx < len(mce.cause); idx++ {
		errBuilder.WriteString(", ")
		errBuilder.WriteString(mce.cause[idx].Error())
	}
	return errBuilder.String()
}
