package processor

import (
	"sync"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"

	"gitlab.com/abhishp/rain-csv-parser/pkg/csv"
	"gitlab.com/abhishp/rain-csv-parser/pkg/decoder"
	"gitlab.com/abhishp/rain-csv-parser/pkg/domain"
)

type Processor interface {
	Process(Metadata)
}

type SuccessCallback func(record domain.Record, index int64)

type ErrorCallback func(csvRow []string, index int64, err error)

type processor struct {
	reader          csv.Reader
	decoder         decoder.Decoder
	successCallback stateCallback
	errorCallback   stateCallback
	wg              *sync.WaitGroup
	metadata        Metadata
}

type executionState struct {
	row    []string
	record domain.Record
	err    error
	index  int64
}

type stateCallback func(*executionState)

func New(reader csv.Reader, decoder decoder.Decoder, successCallback SuccessCallback, errorCallback ErrorCallback) Processor {
	if successCallback == nil {
		successCallback = defaultSuccessCallback
	}
	if errorCallback == nil {
		errorCallback = defaultErrorCallback
	}
	proc := &processor{
		reader:  reader,
		decoder: decoder,
		wg:      &sync.WaitGroup{},
	}
	proc.wrapCallbacks(successCallback, errorCallback)
	return proc
}

func (p *processor) Process(metadata Metadata) {
	executionStream := make(chan *executionState, 2)
	p.metadata = metadata
	p.wg.Add(2)

	go p.readRecords(executionStream)
	go p.processRecords(executionStream)

	p.wg.Wait()
}

func (p *processor) readRecords(executionStream chan<- *executionState) {
	failureChannel := make(chan *executionState, 2)
	p.wg.Add(1)
	defer p.cleanup(executionStream, failureChannel)
	go p.handle(failureChannel, p.errorCallback)

	log.Debug().Msg("Starting to read records")
	for {
		csvRow, err := p.reader.Read()
		if err == csv.EOF {
			log.Debug().Msg("Read all records from CSV")
			return
		}
		p.metadata.IncrementTotal()
		state := &executionState{index: p.metadata.Total()}
		if err != nil {
			state.err = err
			failureChannel <- state
		} else if len(csvRow) > 0 {
			log.Debug().Strs("record", csvRow).Msg("read from file")
			state.row = csvRow
			executionStream <- state
		}
	}
}

func (p *processor) processRecords(executionStream <-chan *executionState) {
	successChannel := make(chan *executionState, 2)
	failuresChannel := make(chan *executionState, 2)

	defer func() {
		p.wg.Done()
		close(successChannel)
		close(failuresChannel)
	}()

	p.wg.Add(2)
	go p.handle(successChannel, p.successCallback)
	go p.handle(failuresChannel, p.errorCallback)

	wg := &sync.WaitGroup{}
	for state := range executionStream {
		wg.Add(1)
		go func(execState *executionState) {
			defer wg.Done()
			execState.err = p.doProcessRecord(execState)
			if execState.err != nil {
				failuresChannel <- execState
			} else {
				successChannel <- execState
			}
		}(state)
	}
	wg.Wait()
}

func (p *processor) doProcessRecord(state *executionState) error {
	record, err := p.decoder.Decode(state.row)
	if err != nil {
		return err
	}
	es := make([]error, 0, len(record))
	for attribute, value := range record {
		err := value.Validate()
		if err == nil {
			err = attribute.ValidateValue(value)
		}
		if err != nil {
			es = append(es, errors.Wrap(err, attribute.OutputHeader))
		}
	}
	if len(es) != 0 {
		return NewMultiCauseError("validation error", es...)
	}
	state.record = record
	return nil
}

func (p *processor) handle(executionStream <-chan *executionState, callback stateCallback) {
	defer p.cleanup()

	for state := range executionStream {
		p.wg.Add(1)
		go callback(state)
	}
}

func (p *processor) cleanup(channels ...chan<- *executionState) {
	for _, channel := range channels {
		close(channel)
	}
	p.wg.Done()
}

func (p *processor) wrapCallbacks(successCallback SuccessCallback, errorCallback ErrorCallback) {
	p.successCallback = func(state *executionState) {
		defer p.cleanup()
		p.metadata.IncrementSuccess()
		log.Debug().Msgf("success callback called for record#%d", state.index)
		successCallback(state.record, state.index)
	}
	p.errorCallback = func(state *executionState) {
		defer p.cleanup()
		p.metadata.IncrementFailure()
		log.Debug().Err(state.err).Msgf("error callback called for record#%d", state.index)
		errorCallback(state.row, state.index, state.err)
	}
}

func defaultErrorCallback(csvRow []string, index int64, err error) {
	log.Error().Err(err).Msgf("failed to process record %v at index %d", csvRow, index)
}

func defaultSuccessCallback(record domain.Record, index int64) {
	successEntry := log.Info()
	for attribute, value := range record {
		successEntry.Stringer(attribute.OutputHeader, value)
	}
	successEntry.Msgf("record processed at %d", index)
}
