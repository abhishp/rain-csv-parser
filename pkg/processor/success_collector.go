package processor

import (
	"encoding/csv"
	"io"
	"sync"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"

	"gitlab.com/abhishp/rain-csv-parser/pkg/domain"
)

type SuccessCollector interface {
	Collect(record domain.Record, index int64)
	io.Closer
}

type successCollector struct {
	attributes domain.Attributes
	writer     *csv.Writer
	mutex      sync.Mutex
}

func NewSuccessCollector(attributes domain.Attributes, csvFile io.Writer) (SuccessCollector, error) {
	writer := csv.NewWriter(csvFile)
	successHeader := make([]string, len(attributes))
	for idx, attribute := range attributes {
		successHeader[idx] = attribute.OutputHeader
	}
	if err := writer.Write(successHeader); err != nil {
		return nil, errors.Wrapf(err, "failed to write header for success csv")
	}
	return &successCollector{
		attributes: attributes,
		writer:     writer,
	}, nil
}

func (s *successCollector) Collect(record domain.Record, index int64) {
	csvRecord := make([]string, len(s.attributes))
	for idx, attribute := range s.attributes {
		csvRecord[idx] = record[attribute].String()
	}
	s.mutex.Lock()
	defer s.mutex.Unlock()
	logger := log.With().Int64("index", index).Strs("record", csvRecord).Logger()
	if err := s.writer.Write(csvRecord); err != nil {
		logger.Error().Err(err).Msg("error while writing success record")
		return
	}
	logger.Debug().Msg("wrote success record")
}

func (s *successCollector) Close() error {
	s.writer.Flush()
	err := errors.Wrap(s.writer.Error(), "failed to close success collector writer")
	log.Debug().Err(err).Msg("closed success collector")
	return err
}
