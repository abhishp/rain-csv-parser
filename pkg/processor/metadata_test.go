package processor_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"

	"gitlab.com/abhishp/rain-csv-parser/pkg/processor"
)

type MetadataTestSuite struct {
	suite.Suite
	metadata processor.Metadata
}

func TestMetadata(t *testing.T) {
	suite.Run(t, new(MetadataTestSuite))
}

func (suite *MetadataTestSuite) SetupTest() {
	suite.metadata = processor.NewMetadata()
}

const (
	separator            = "+----------------------------+\n"
	defaultSummaryString = "         Summary          "
)

func (suite *MetadataTestSuite) TestIncrementTotal() {
	suite.Run("should increment the failure count for metadata", func() {
		suite.metadata.IncrementTotal()
		suite.assertMetadata(1, 0, 0, suite.metadata)

		suite.metadata.IncrementTotal()
		suite.assertMetadata(2, 0, 0, suite.metadata)
	})
}

func (suite *MetadataTestSuite) TestIncrementSuccess() {
	suite.Run("should increment the success count for metadata", func() {
		suite.metadata.IncrementSuccess()
		suite.assertMetadata(0, 1, 0, suite.metadata)

		suite.metadata.IncrementSuccess()
		suite.metadata.IncrementSuccess()
		suite.assertMetadata(0, 3, 0, suite.metadata)
	})
}

func (suite *MetadataTestSuite) TestIncrementFailure() {
	suite.Run("should increment the failure count for metadata", func() {
		suite.metadata.IncrementFailure()
		suite.assertMetadata(0, 0, 1, suite.metadata)

		suite.metadata.IncrementFailure()
		suite.metadata.IncrementFailure()
		suite.metadata.IncrementFailure()
		suite.assertMetadata(0, 0, 4, suite.metadata)
	})
}

func (suite *MetadataTestSuite) TestFormat() {
	suite.Run("should return string representation for metadata", func() {
		metadata := suite.buildMetadataFor("", 5, 2, 3)

		expectedMetaString := suite.buildExpectedMetaString(defaultSummaryString, 5, 2, 3)

		suite.Equal(expectedMetaString, fmt.Sprintf("%v", metadata))
		suite.Equal(expectedMetaString, fmt.Sprint(metadata))
	})

	suite.Run("empty metadata", func() {
		metadata := processor.NewMetadataWithTitle("Custom Long Title")
		expectedMetaString := fmt.Sprintf("%[1]s|     Custom Long Title      |\n%[1]s|    No records processed    |\n%[1]s", separator)

		suite.Equal(expectedMetaString, fmt.Sprint(metadata))
	})

	suite.Run("with details", func() {
		metadata := processor.NewMetadataWithTitle("Custom Long Title")
		metadataOne := suite.buildMetadataFor("Category 1", 10, 7, 3)
		metadataTwo := suite.buildMetadataFor("Category 2", 30, 25, 5)
		expectedMetaString :=
			suite.buildExpectedMetaString("    Custom Long Title     ", 40, 32, 8) +
				"\n           DETAILS            \n\n" +
				suite.buildExpectedMetaString("        Category 1        ", 10, 7, 3) +
				suite.buildExpectedMetaString("        Category 2        ", 30, 25, 5)

		metadata.AddDetails(metadataOne, metadataTwo)

		suite.Equal(expectedMetaString, fmt.Sprint(metadata))
		suite.Equal(expectedMetaString, fmt.Sprintf("%v", metadata))
	})

	suite.Run("with empty details", func() {
		metadata := processor.NewMetadata()
		metadataOne := processor.NewMetadataWithTitle("Category 1")
		metadataTwo := suite.buildMetadataFor("Category 2", 30, 25, 5)
		expectedMetaString :=
			suite.buildExpectedMetaString(defaultSummaryString, 30, 25, 5) +
				fmt.Sprintf("\n           DETAILS            \n\n%[1]s|         Category 1         |\n%[1]s", separator) +
				fmt.Sprintf("|    No records processed    |\n%s", separator) +
				suite.buildExpectedMetaString("        Category 2        ", 30, 25, 5)

		metadata.AddDetails(metadataOne, metadataTwo)

		suite.Equal(expectedMetaString, fmt.Sprint(metadata))
		suite.Equal(expectedMetaString, fmt.Sprintf("%v", metadata))
	})
}

func (suite *MetadataTestSuite) TestTitle() {
	suite.Run("default title", func() {
		metadata := processor.NewMetadata()
		metadata.IncrementTotal()
		metadata.IncrementSuccess()
		expectedMetaString := suite.buildExpectedMetaString(defaultSummaryString, 1, 1, 0)

		suite.Equal("Summary", metadata.Title())
		suite.Equal(expectedMetaString, fmt.Sprint(metadata))
	})

	suite.Run("custom title", func() {
		customTitle := "Custom Long Title"
		metadata := suite.buildMetadataFor(customTitle, 150, 141, 9)
		expectedMetaString := suite.buildExpectedMetaString("    Custom Long Title     ", 150, 141, 9)

		suite.Equal(customTitle, metadata.Title())
		suite.Equal(expectedMetaString, fmt.Sprint(metadata))
	})
}

func (suite *MetadataTestSuite) TestAddDetails() {
	suite.Run("consolidate the metadata into the original metadata object", func() {
		metadata := processor.NewMetadata()
		metadataOne := suite.buildMetadataFor("Category 1", 10, 7, 3)
		metadataTwo := suite.buildMetadataFor("Category 2", 30, 25, 5)

		metadata.AddDetails(metadataOne, metadataTwo)

		suite.assertMetadata(40, 32, 8, metadata)
	})

	suite.Run("with empty details", func() {
		metadata := processor.NewMetadata()
		metadataOne := suite.buildMetadataFor("Category 1", 10, 7, 3)
		metadataTwo := suite.buildMetadataFor("Category 2", 0, 25, 5)

		metadata.AddDetails(metadataOne, metadataTwo)

		suite.assertMetadata(10, 7, 3, metadata)
	})
}

func (suite *MetadataTestSuite) assertMetadata(total, successCount, failureCount int64, metadata processor.Metadata) {
	suite.Equal(total, metadata.Total())
	suite.Equal(successCount, metadata.SuccessCount())
	suite.Equal(failureCount, metadata.FailureCount())
}

func (suite *MetadataTestSuite) buildExpectedMetaString(title string, total, success, error int) string {
	summary := fmt.Sprintf("| %s |\n", title)
	totalStr := fmt.Sprintf("| Total:                 %3d |\n", total)
	successStr := fmt.Sprintf("| Success:               %3d |\n", success)
	failureStr := fmt.Sprintf("| Failures:              %3d |\n", error)
	return separator + summary + separator + totalStr + successStr + failureStr + separator
}

func (suite *MetadataTestSuite) buildMetadataFor(title string, total, success, error int) (meta processor.Metadata) {
	if title == "" {
		title = "Summary"
	}
	meta = processor.NewMetadataWithTitle(title)
	for idx := 0; idx < total; idx++ {
		meta.IncrementTotal()
		if idx < success {
			meta.IncrementSuccess()
		}
		if idx < error {
			meta.IncrementFailure()
		}
	}
	return meta
}
