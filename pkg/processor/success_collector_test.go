package processor_test

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"strings"
	"testing"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/suite"

	"gitlab.com/abhishp/rain-csv-parser/pkg/domain"
	"gitlab.com/abhishp/rain-csv-parser/pkg/domain/types"
	"gitlab.com/abhishp/rain-csv-parser/pkg/processor"
	"gitlab.com/abhishp/rain-csv-parser/test/stubs"
)

type SuccessCollectorTestSuite struct {
	suite.Suite
	attributes       domain.Attributes
	originalLogLevel zerolog.Level
}

func TestSuccessCollector(t *testing.T) {
	suite.Run(t, new(SuccessCollectorTestSuite))
}

func (suite *SuccessCollectorTestSuite) SetupSuite() {
	suite.originalLogLevel = zerolog.GlobalLevel()
	zerolog.SetGlobalLevel(zerolog.ErrorLevel)
	suite.attributes = domain.Attributes{
		{Name: "attr1", OutputHeader: "Attribute One"},
		{Name: "attr2", OutputHeader: "Attribute Two"},
		{Name: "attr3", OutputHeader: "Attribute Three"},
	}
}

func (suite *SuccessCollectorTestSuite) TearDownSuite() {
	zerolog.SetGlobalLevel(suite.originalLogLevel)
}

func (suite *SuccessCollectorTestSuite) TestNewSuccessCollector() {
	suite.Run("should write header to csv file", func() {
		buffer := bytes.NewBuffer(make([]byte, 0, 50))

		writer := bufio.NewWriter(buffer)

		collector, err := processor.NewSuccessCollector(suite.attributes, writer)

		suite.NotNil(collector)
		suite.Implements((*processor.SuccessCollector)(nil), collector)
		suite.Nil(err)
		suite.Require().Nil(writer.Flush())
		suite.Equal("Attribute One,Attribute Two,Attribute Three\n", buffer.String())
	})
}

func (suite *SuccessCollectorTestSuite) TestSuccessCollector_Collect() {
	suite.Run("should write the record in correct order", func() {
		buffer := bytes.NewBuffer(make([]byte, 0, 50))
		writer := bufio.NewWriter(buffer)
		collector, err := processor.NewSuccessCollector(suite.attributes, writer)
		suite.Require().Nil(err)
		writer.Reset(buffer)
		record := domain.Record{
			suite.attributes[2]: suite.buildStringValue("value attr 3"),
			suite.attributes[0]: suite.buildStringValue("value attr 1"),
			suite.attributes[1]: suite.buildStringValue("value attr 2"),
		}

		collector.Collect(record, 5)

		suite.Equal(nil, writer.Flush())
		suite.Equal("value attr 1,value attr 2,value attr 3\n", buffer.String())
	})

	suite.Run("should handle error while writing record the record in correct order", func() {
		logBuffer := bytes.NewBuffer([]byte{})
		defer func(logger zerolog.Logger) {
			log.Logger = logger
		}(log.Logger)
		log.Logger = log.Logger.Output(logBuffer)

		errorWriter := stubs.NewFixedLengthBuffer(10)
		writer := bufio.NewWriter(errorWriter)
		collector, err := processor.NewSuccessCollector(suite.attributes, writer)
		suite.Require().Nil(err)

		record := domain.Record{}
		values := [3]types.Value{}
		for idx := range suite.attributes {
			values[idx] = suite.buildStringValue(strings.Repeat(fmt.Sprintf("value attr %d", idx), 150))
			record[suite.attributes[idx]] = values[idx]
		}

		collector.Collect(record, 5)

		suite.Equal(io.ErrShortBuffer, writer.Flush())
		suite.Contains(logBuffer.String(), `"level":"error","index":5,`)
		suite.Contains(logBuffer.String(), `"error":"short buffer"`)
		suite.Contains(logBuffer.String(), `"message":"error while writing success record"`)
	})
}

func (suite *SuccessCollectorTestSuite) TestSuccessCollector_Close() {
	suite.Run("should write any data in the buffer to the writer", func() {
		buffer := bytes.NewBuffer(make([]byte, 0, 50))
		writer := bufio.NewWriter(buffer)
		collector, err := processor.NewSuccessCollector(suite.attributes, writer)
		suite.Require().Nil(err)
		suite.Empty(buffer.String())
		record := domain.Record{}
		for idx := range suite.attributes {
			record[suite.attributes[idx]] = suite.buildStringValue(fmt.Sprintf("value attr %d", idx+1))
		}

		collector.Collect(record, 5)
		suite.Empty(buffer.String())

		err = collector.Close()

		suite.Nil(err)
		suite.Equal("Attribute One,Attribute Two,Attribute Three\nvalue attr 1,value attr 2,value attr 3\n", buffer.String())
	})

	suite.Run("should return error returned while writing", func() {
		writer := stubs.NewFixedLengthBuffer(50)
		collector, err := processor.NewSuccessCollector(suite.attributes, writer)
		suite.Require().Nil(err)
		suite.Empty(writer.String())
		record := domain.Record{}
		for idx := range suite.attributes {
			record[suite.attributes[idx]] = suite.buildStringValue(fmt.Sprintf("value attr %d", idx+1))
		}

		collector.Collect(record, 5)
		suite.Empty(writer.String())

		err = collector.Close()

		suite.Require().NotNil(err)
		suite.Equal("failed to close success collector writer: short buffer", err.Error())
		suite.Equal("Attribute One,Attribute Two,Attribute Three\nvalue ", writer.String())
	})
}

func (suite *SuccessCollectorTestSuite) buildStringValue(s string) types.Value {
	str := new(types.String)
	_ = str.UnmarshalText(s)
	return str
}
