package processor

import (
	"encoding/csv"
	"io"
	"strconv"
	"sync"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
)

type ErrorCollector interface {
	Collect(csvRow []string, index int64, err error)
	io.Closer
}

type errorCollector struct {
	writer *csv.Writer
	mutex  sync.Mutex
}

func NewErrorCollector(inputHeaders []string, csvFile io.Writer) (ErrorCollector, error) {
	failureHeader := append(inputHeaders, "Index", "Error")
	writer := csv.NewWriter(csvFile)
	if err := writer.Write(failureHeader); err != nil {
		return nil, errors.Wrapf(err, "failure while writing header for failure csv")
	}
	return &errorCollector{
		writer: writer,
	}, nil
}

func (c *errorCollector) Collect(csvRow []string, index int64, err error) {
	errorRecord := append(csvRow, strconv.FormatInt(index, 10), err.Error())
	c.mutex.Lock()
	defer c.mutex.Unlock()
	logger := log.With().Int64("index", index).Strs("record", errorRecord).Logger()
	if err := c.writer.Write(errorRecord); err != nil {
		logger.Err(err).Msg("error while writing error record")
		return
	}
	logger.Debug().Msg("wrote failure record")
}

func (c *errorCollector) Close() (err error) {
	c.writer.Flush()
	err = errors.Wrap(c.writer.Error(), "failed to close error collector writer")
	log.Debug().Err(err).Msg("closed error collector")
	return err
}
