package processor_test

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
	"strings"
	"testing"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/suite"

	"gitlab.com/abhishp/rain-csv-parser/pkg/processor"
	"gitlab.com/abhishp/rain-csv-parser/test/stubs"
)

type ErrorCollectorTestSuite struct {
	suite.Suite
	inputHeader      []string
	originalLogLevel zerolog.Level
}

func TestErrorCollector(t *testing.T) {
	suite.Run(t, new(ErrorCollectorTestSuite))
}

func (suite *ErrorCollectorTestSuite) SetupSuite() {
	suite.inputHeader = []string{"attr1", "attr2", "attr3"}
	suite.originalLogLevel = zerolog.GlobalLevel()
	zerolog.SetGlobalLevel(zerolog.ErrorLevel)
}

func (suite *ErrorCollectorTestSuite) TearDownSuite() {
	zerolog.SetGlobalLevel(suite.originalLogLevel)
}

func (suite *ErrorCollectorTestSuite) TestNewErrorCollector() {
	suite.Run("should write header to csv file", func() {
		buffer := bytes.NewBuffer(make([]byte, 0, 50))
		writer := bufio.NewWriter(buffer)

		collector, err := processor.NewErrorCollector(suite.inputHeader, writer)

		suite.NotNil(collector)
		suite.Implements((*processor.ErrorCollector)(nil), collector)
		suite.Nil(err)
		suite.Require().Nil(writer.Flush())
		suite.Equal("attr1,attr2,attr3,Index,Error\n", buffer.String())
	})
}

func (suite *ErrorCollectorTestSuite) TestErrorCollector_Collect() {
	suite.Run("should write the record in correct order", func() {
		buffer := bytes.NewBuffer(make([]byte, 0, 50))
		writer := bufio.NewWriter(buffer)
		collector, err := processor.NewErrorCollector(suite.inputHeader, writer)
		suite.Require().Nil(err)
		writer.Reset(buffer)
		record := []string{"value attr 1", "value attr 2", "value attr 3"}

		collector.Collect(record, 5, errors.New("some error"))

		suite.Equal(nil, writer.Flush())
		suite.Equal("value attr 1,value attr 2,value attr 3,5,some error\n", buffer.String())
	})

	suite.Run("should handle error while writing record the record in correct order", func() {
		logBuffer := bytes.NewBuffer([]byte{})
		defer func(logger zerolog.Logger) {
			log.Logger = logger
		}(log.Logger)
		log.Logger = log.Logger.Output(logBuffer)

		errorWriter := stubs.NewFixedLengthBuffer(10)
		writer := bufio.NewWriter(errorWriter)
		collector, err := processor.NewErrorCollector(suite.inputHeader, writer)
		suite.Require().Nil(err)
		record := make([]string, 3)
		for idx := range suite.inputHeader {
			record[idx] = strings.Repeat(fmt.Sprintf("value attr %d", idx), 150)
		}

		collector.Collect(record, 2, errors.New("some other error"))

		suite.Equal(io.ErrShortBuffer, writer.Flush())
		suite.Contains(logBuffer.String(), `"level":"error","index":2,`)
		suite.Contains(logBuffer.String(), `"error":"short buffer"`)
		suite.Contains(logBuffer.String(), `"message":"error while writing error record"`)
	})
}

func (suite *ErrorCollectorTestSuite) TestErrorCollector_Close() {
	header := []string{"Name", "Email ID"}

	suite.Run("should write any data in the buffer to the writer", func() {
		buffer := bytes.NewBuffer(make([]byte, 0, 50))
		writer := bufio.NewWriter(buffer)
		collector, err := processor.NewErrorCollector(header, writer)
		suite.Require().Nil(err)
		suite.Empty(buffer.String())
		record := []string{"test", ""}

		collector.Collect(record, 1, errors.New("record error: empty email"))
		suite.Empty(buffer.String())

		err = collector.Close()

		suite.Nil(err)
		suite.Equal("Name,Email ID,Index,Error\ntest,,1,record error: empty email\n", buffer.String())
	})

	suite.Run("should return error returned while writing", func() {
		writer := stubs.NewFixedLengthBuffer(20)
		collector, err := processor.NewErrorCollector(header, writer)
		suite.Require().Nil(err)
		suite.Empty(writer.String())

		err = collector.Close()

		suite.Require().NotNil(err)
		suite.Equal("failed to close error collector writer: short buffer", err.Error())
		suite.Equal("Name,Email ID,Index,", writer.String())
	})
}
