package processor

import (
	"fmt"
	"strings"
	"sync"
	"text/template"

	"github.com/rs/zerolog/log"
)

var metadataTemplate *template.Template

func init() {
	title := "| {{ .Title | center 26 }} |"
	separator := "+----------------------------+"
	total := `| Total: {{printf "%19d" .Total}} |`
	success := `| Success: {{printf "%17d" .SuccessCount}} |`
	failure := `| Failures: {{printf "%16d" .FailureCount}} |`
	emptyDetails := `| {{ "No records processed" | center 26 }} |`
	metadataTemplateParts := []string{separator, title, separator, total, success, failure, separator, ""}
	emptyMetadataTemplateParts := []string{separator, title, separator, emptyDetails, separator, ""}
	metadataTemplate = template.Must(
		template.New(metadataTemplateName).
			Funcs(template.FuncMap{"center": centerString}).
			Parse(strings.Join(metadataTemplateParts, "\n")),
	)
	template.Must(
		metadataTemplate.New(emptyMetadataTemplateName).
			Parse(strings.Join(emptyMetadataTemplateParts, "\n")),
	)
}

const (
	metadataTemplateName      = "metadata"
	emptyMetadataTemplateName = "emptyMetadata"
)

type Metadata interface {
	IncrementTotal()
	IncrementSuccess()
	IncrementFailure()
	Total() int64
	SuccessCount() int64
	FailureCount() int64
	AddDetails(metas ...Metadata)
	Title() string
	Details() []Metadata
	fmt.Formatter
}

type metadata struct {
	failureCount int64
	successCount int64
	total        int64
	title        string
	details      []Metadata
	mutex        sync.Mutex
}

func NewMetadata() Metadata {
	return &metadata{title: "Summary"}
}

func NewMetadataWithTitle(title string) Metadata {
	return &metadata{title: title}
}

func (m *metadata) IncrementTotal() {
	m.mutex.Lock()
	defer m.mutex.Unlock()
	m.total++
}

func (m *metadata) IncrementSuccess() {
	m.mutex.Lock()
	defer m.mutex.Unlock()
	m.successCount++
}

func (m *metadata) IncrementFailure() {
	m.mutex.Lock()
	defer m.mutex.Unlock()
	m.failureCount++
}

func (m *metadata) Format(state fmt.State, r rune) {
	t := metadataTemplateName
	if m.Total() == 0 {
		t = emptyMetadataTemplateName
	}
	if err := metadataTemplate.ExecuteTemplate(state, t, m); err != nil {
		log.Error().Err(err).Msg("error while formatting metadata")
	}
	if len(m.details) > 0 {
		writeToState(state, "\n%s\n\n", centerString(30, "DETAILS"))
		for _, detail := range m.details {
			detail.Format(state, r)
		}
	}
}

func (m *metadata) Total() int64 {
	return m.total
}

func (m *metadata) SuccessCount() int64 {
	return m.successCount
}

func (m *metadata) FailureCount() int64 {
	return m.failureCount
}

func (m *metadata) AddDetails(metas ...Metadata) {
	if len(metas) == 0 {
		return
	}

	for _, meta := range metas {
		if meta.Total() == 0 {
			continue
		}
		m.total += meta.Total()
		m.failureCount += meta.FailureCount()
		m.successCount += meta.SuccessCount()
	}
	m.details = append(m.details, metas...)
}

func (m *metadata) Title() string {
	return m.title
}

func (m *metadata) Details() []Metadata {
	return m.details
}

func centerString(width int, str string) string {
	n := width - len([]rune(str))
	if n <= 0 {
		return str
	}

	left := n / 2
	right := n - left
	centraliser := &strings.Builder{}
	centraliser.Grow(width)
	space := " "
	centraliser.WriteString(strings.Repeat(space, left))
	centraliser.WriteString(str)
	centraliser.WriteString(strings.Repeat(space, right))
	return centraliser.String()
}

func writeToState(f fmt.State, format string, args ...interface{}) {
	if _, err := fmt.Fprintf(f, format, args...); err != nil {
		log.Error().Err(err).Msg("error while formatting metadata")
	}
}
