package csv

import (
	"fmt"
	"os"

	"github.com/pkg/errors"
)

type File = *os.File

func Open(csvPath string) (File, error) {
	csv, err := os.Open(csvPath)
	if err != nil {
		return nil, errors.Wrapf(err, "can't open csv file %s", csvPath)
	}

	if stats, _ := csv.Stat(); stats.IsDir() || stats.Size() == 0 {
		return nil, fmt.Errorf("invalid or empty csv file %s", csvPath)
	}

	return csv, nil
}
