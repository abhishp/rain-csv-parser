package csv_test

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/suite"

	"gitlab.com/abhishp/rain-csv-parser/pkg/csv"
)

type CSVFileTestSuite struct {
	suite.Suite
}

type fileTestFunc func(tempFile *os.File)

func TestCSVFile(t *testing.T) {
	suite.Run(t, new(CSVFileTestSuite))
}

func (suite CSVFileTestSuite) TestNewFile() {
	suite.Run("return error when no file exists at the path", func() {
		invalidPaths := []string{"", "__some_invalid_file_path__.csv"}
		for _, invalidPath := range invalidPaths {
			csvFile, err := csv.Open(invalidPath)

			suite.Nil(csvFile)
			suite.NotNil(err)
			suite.Contains(err.Error(), fmt.Sprintf("can't open csv file %s", invalidPath))
		}
	})

	suite.Run("return error when the path given is a directory", func() {
		pwd, _ := os.Getwd()
		csvFile, err := csv.Open(pwd)

		suite.Nil(csvFile)
		suite.NotNil(err)
		suite.Contains(err.Error(), fmt.Sprintf("invalid or empty csv file %s", pwd))
	})

	suite.Run("return error when the given file is empty", func() {
		suite.withTempFile("", func(tempFile *os.File) {
			csvFile, err := csv.Open(tempFile.Name())

			suite.Nil(csvFile)
			suite.NotNil(err)
			suite.Contains(err.Error(), fmt.Sprintf("invalid or empty csv file %s", tempFile.Name()))
		})
	})

	suite.Run("return no error when the given file is not empty", func() {
		suite.withTempFile("fieldOne,fieldTwo", func(tempFile *os.File) {
			csvFile, err := csv.Open(tempFile.Name())

			suite.IsType((csv.File)(nil), csvFile)
			suite.Nil(err)
		})
	})
}

func (suite CSVFileTestSuite) withTempFile(content string, testFunc fileTestFunc) {
	file, _ := os.CreateTemp("", "file_test_csv")
	suite.Require().NotNil(file)
	defer func() {
		_ = file.Close()
		_ = os.Remove(file.Name())
	}()
	_, err := file.WriteString(content)
	suite.Require().Nil(err)
	testFunc(file)
}
