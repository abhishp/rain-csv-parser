package csv_test

import (
	"fmt"
	"io"
	"strings"
	"testing"
	"testing/iotest"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/suite"

	"gitlab.com/abhishp/rain-csv-parser/test/stubs"

	"gitlab.com/abhishp/rain-csv-parser/pkg/csv"
)

type CSVReaderTestSuite struct {
	suite.Suite
}

func TestCSVReader(t *testing.T) {
	suite.Run(t, new(CSVReaderTestSuite))
}

func (suite CSVReaderTestSuite) TestNewReader() {
	suite.Run("returns error when source reader errors out", func() {
		expectedError := errors.New("some io read error")
		reader, err := csv.NewReader(io.NopCloser(iotest.ErrReader(expectedError)))

		suite.Nil(reader)
		suite.Equal("can't read headers: some io read error", err.Error())
	})

	suite.Run("returns no error when csv is not empty", func() {
		reader, err := csv.NewReader(io.NopCloser(strings.NewReader("field1,field2\nvalue1,2")))

		suite.NotNil(reader)
		suite.Nil(err)
	})
}

func (suite CSVReaderTestSuite) TestRead() {
	suite.Run("returns error when source reached end of file", func() {
		source := stubs.NewReadCloser(strings.NewReader("someContent"))
		reader, err := csv.NewReader(source)
		suite.Require().Nil(err)

		record, err := reader.Read()

		suite.Nil(record)
		suite.Equal(csv.EOF, err)
		suite.True(source.IsClosed)
	})

	suite.Run("returns error when record does not have same number of fields as the headers", func() {
		errorSource := io.NopCloser(strings.NewReader(`fieldOne,fieldTwo
																			r1v1
																			r3v1,r3v2,r3v3`))
		reader, err := csv.NewReader(errorSource)
		suite.Require().Nil(err)

		for lineNo := 2; lineNo < 4; lineNo++ {
			record, err := reader.Read()

			suite.Nil(record)
			suite.NotNil(err)
			suite.Equal(fmt.Sprintf("can't read record: record on line %d: wrong number of fields", lineNo), err.Error())
		}
	})

	suite.Run("returns record", func() {
		testCases := map[string]struct {
			content        string
			expectedRecord []string
		}{
			"when records present with same number of fields": {
				content:        "fieldOne,fieldTwo\nvalueOne,valueTwo",
				expectedRecord: []string{"valueOne", "valueTwo"},
			},
			"when spaces are present should ignore leading and trailing spaces": {
				content:        "fieldOne, fieldTwo\n  valueOne,     valueTwoWithTrailingSpace     ",
				expectedRecord: []string{"valueOne", "valueTwoWithTrailingSpace"},
			},
			"when empty field values are present": {
				content:        "fieldOne, fieldTwo\n  ,     valueTwo",
				expectedRecord: []string{"", "valueTwo"},
			},
		}

		for title, testCase := range testCases {
			suite.Run(title, func() {
				reader, err := csv.NewReader(io.NopCloser(strings.NewReader(testCase.content)))
				suite.Require().Nil(err)

				record, err := reader.Read()

				suite.Nil(err)
				suite.Equal(testCase.expectedRecord, record)
			})
		}
	})

	suite.Run("multiple read operations", func() {
		source := io.NopCloser(strings.NewReader("fieldOne,fieldTwo\nr1v1,r1v2\nr2v1,r2v2\nr3v1,r3v2"))
		reader, err := csv.NewReader(source)
		suite.Require().Nil(err)
		expectedCSVRecords := [3][]string{
			{"r1v1", "r1v2"},
			{"r2v1", "r2v2"},
			{"r3v1", "r3v2"},
		}

		actualCSVRecords := [3][]string{}

		for idx := range expectedCSVRecords {
			record, err := reader.Read()

			suite.Require().Nil(err)
			actualCSVRecords[idx] = record
		}

		suite.Equal(expectedCSVRecords, actualCSVRecords)
	})
}

func (suite CSVReaderTestSuite) TestHeader() {
	suite.Run("returns the header row with all the values with trimmed spaces", func() {
		source := io.NopCloser(strings.NewReader(`fieldOne ,    fieldTwo , fieldThree`))
		reader, err := csv.NewReader(source)
		suite.Require().Nil(err)

		suite.Equal([]string{"fieldOne", "fieldTwo", "fieldThree"}, reader.Header())
	})
}
