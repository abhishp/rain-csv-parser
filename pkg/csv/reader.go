package csv

import (
	"encoding/csv"
	"io"
	"strings"

	"github.com/dimchansky/utfbom"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
)

type Reader interface {
	Read() ([]string, error)
	Header() []string
}

var EOF = io.EOF

type reader struct {
	headers    []string
	csv        *csv.Reader
	closerFunc func()
}

func NewReader(source io.ReadCloser) (Reader, error) {
	csvReader := csv.NewReader(utfbom.SkipOnly(source))
	csvReader.TrimLeadingSpace = true
	headers, err := csvReader.Read()
	if err != nil {
		return nil, errors.Wrap(err, "can't read headers")
	}
	return &reader{
		csv:        csvReader,
		headers:    trimStringSlice(headers),
		closerFunc: wrapCloser(source.Close),
	}, nil
}

func (cfr reader) Read() (record []string, err error) {
	record, err = cfr.csv.Read()
	if err == io.EOF {
		defer cfr.closerFunc()
		return nil, EOF
	}
	if err != nil {
		return nil, errors.Wrap(err, "can't read record")
	}
	return trimStringSlice(record), nil
}

func (cfr *reader) Header() []string {
	return cfr.headers
}

func trimStringSlice(slice []string) []string {
	if slice == nil {
		return nil
	}
	for idx := range slice {
		slice[idx] = strings.TrimSpace(slice[idx])
	}
	return slice
}

func wrapCloser(closer func() error) func() {
	return func() {
		if err := closer(); err != nil {
			log.Error().Err(err).Msg("error while closing the source")
		}
	}
}
