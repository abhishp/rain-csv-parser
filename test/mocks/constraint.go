package mocks

import (
	"github.com/stretchr/testify/mock"

	"gitlab.com/abhishp/rain-csv-parser/pkg/domain/types"
)

type Constraint struct {
	mock.Mock
}

func (c *Constraint) Validate(value types.Value) error {
	call := c.Called(value)
	return call.Error(0)
}
