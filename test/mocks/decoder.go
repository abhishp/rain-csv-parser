package mocks

import (
	"github.com/stretchr/testify/mock"

	"gitlab.com/abhishp/rain-csv-parser/pkg/domain"
)

type Decoder struct {
	mock.Mock
}

func (d *Decoder) Decode(record []string) (domain.Record, error) {
	call := d.Called(record)
	r, _ := call.Get(0).(domain.Record)
	return r, call.Error(1)
}
