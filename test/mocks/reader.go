package mocks

import "github.com/stretchr/testify/mock"

type Reader struct {
	mock.Mock
}

func (r *Reader) Read() ([]string, error) {
	call := r.Called()
	strings, _ := call.Get(0).([]string)
	return strings, call.Error(1)
}

func (r *Reader) Header() []string {
	call := r.Called()
	header, _ := call.Get(0).([]string)
	return header
}
