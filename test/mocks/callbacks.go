package mocks

import (
	"github.com/stretchr/testify/mock"

	"gitlab.com/abhishp/rain-csv-parser/pkg/domain"
)

type ProcessorCallback struct {
	mock.Mock
}

func (pc *ProcessorCallback) SuccessCallback(record domain.Record, index int64) {
	pc.Called(record, index)
}

func (pc *ProcessorCallback) ErrorCallback(csvRow []string, index int64, err error) {
	pc.Called(csvRow, index, err)
}
