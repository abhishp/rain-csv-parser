package mocks

import "github.com/stretchr/testify/mock"

type Value struct {
	mock.Mock
}

func (v *Value) UnmarshalText(value string) error {
	call := v.Called(value)
	return call.Error(0)
}

func (v *Value) Validate() error {
	return v.Called().Error(0)
}

func (v *Value) String() string {
	return v.Called().String(0)
}
