package stubs

import (
	"io"
)

type FixedLengthBuffer struct {
	length int
	total  int
	buf    []byte
}

func NewFixedLengthBuffer(length int) *FixedLengthBuffer {
	return &FixedLengthBuffer{
		length: length,
		buf:    make([]byte, 0, length),
	}
}

func (w *FixedLengthBuffer) Write(data []byte) (n int, err error) {
	w.buf = append(w.buf, data[:w.length-w.total]...)
	w.total += len(data)
	if w.total > w.length {
		return n, io.ErrShortBuffer
	}
	return len(data), nil
}

func (w *FixedLengthBuffer) String() string {
	return string(w.buf)
}
