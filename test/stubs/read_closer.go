package stubs

import "io"

type ReadCloser struct {
	io.Reader
	IsClosed bool
}

func NewReadCloser(reader io.Reader) *ReadCloser {
	return &ReadCloser{Reader: reader}
}

func (rc *ReadCloser) Close() error {
	rc.IsClosed = true
	return nil
}
