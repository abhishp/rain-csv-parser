# HELP sourced from https://gist.github.com/prwhite/8168133

# Add help text after each target name starting with '\#\#'
# A category can be added with @category
HELP_FUNC = \
	%help; \
	while(<>) { \
		if(/^([a-z0-9_-]+):.*\#\#(?:@(\w+))?\s(.*)$$/) { \
			push(@{$$help{$$2}}, [$$1, $$3]); \
		} \
	}; \
	print "usage: make [target]\n\n"; \
	for ( sort keys %help ) { \
		print "$$_:\n"; \
		printf("  %-30s %s\n", $$_->[0], $$_->[1]) for @{$$help{$$_}}; \
		print "\n"; \
	}

help: ##@help show this help
	@perl -e '$(HELP_FUNC)' $(MAKEFILE_LIST)

export GO111MODULE ?= on
export GOPROXY ?= https://proxy.golang.org
export GOSUMDB ?= sum.golang.org

BUILD_DIR ?= ./build
EXECUTABLE_DIR ?= $(BUILD_DIR)/out
APP_EXECUTABLE = $(EXECUTABLE_DIR)/rain-csv-parser

VERSION=$(shell git describe --always)
LDFLAGS=-ldflags "-w -s -X 'main.version=${VERSION}'"
DEV_LDFLAGS=-ldflags "-w -s -X 'main.version=${VERSION}-dev'"

ALL_PACKAGES=$(shell go list ./pkg/... | grep -v "vendor")

# DEVELOPMENT	###########################################################################################

copy-config: ##@development copy the sample config
	@cp {sample.,}parser.yml

setup-local: copy-config ##@development installs all required dependencies for local development
	go mod tidy
	go mod vendor

# TESTS			###########################################################################################

test: gotest ##@tests run unit tests with coverage
	go mod tidy
	mkdir -p coverage
	gotest -mod=mod -race -v  -cover -coverprofile=coverage/coverage-all.out $(ALL_PACKAGES)


# BUILD			###########################################################################################

clean:
	go clean cmd/rain-csv-parser/*.go
	rm -rf $(EXECUTABLE_DIR)
	go mod tidy

build: clean
	mkdir -p $(EXECUTABLE_DIR)
	go build ${LDFLAGS} -o $(APP_EXECUTABLE) cmd/rain-csv-parser/*.go

# TOOLS			###########################################################################################

include tools/*.mk
