package main

import "github.com/urfave/cli/v2"

var version = "" // Comes from git tag semver

func printVersion(ctx *cli.Context) error {
	cli.ShowVersion(ctx)

	return nil
}
