package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"path"
	"strings"
	"sync"

	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/urfave/cli/v2"

	"gitlab.com/abhishp/rain-csv-parser/configs"
	"gitlab.com/abhishp/rain-csv-parser/pkg/csv"
	"gitlab.com/abhishp/rain-csv-parser/pkg/decoder"
	"gitlab.com/abhishp/rain-csv-parser/pkg/domain"
	"gitlab.com/abhishp/rain-csv-parser/pkg/processor"
)

func process(ctx *cli.Context) (err error) {
	defer handlePanic()
	checkError(configs.Init(ctx.Path("config")))

	log.Logger = log.Level(zerolog.InfoLevel)
	if debugEnabled := ctx.Bool("debug"); debugEnabled {
		log.Logger = log.Level(zerolog.DebugLevel)
	}

	args := ctx.Args()
	if !args.Present() {
		return cli.ShowAppHelp(ctx)
	}

	attributes, err := domain.BuildAttributes(configs.GetAttributes())
	checkError(errors.Wrapf(err, "can not build attributes"))

	successCSV, err := os.Create(ctx.Path("success"))
	checkError(errors.Wrapf(err, "cannot create success output csv file"))

	successCollector, err := processor.NewSuccessCollector(attributes, bufio.NewWriter(successCSV))
	checkError(err)
	defer closeIO(successCollector)

	if args.Len() > 1 {
		return doProcessParallel(args.Slice(), ctx.Path("error-dir"), attributes, successCollector)
	}
	metadata := processor.NewMetadata()
	if err = processCSV(args.First(), ctx.Path("error"), attributes, successCollector, metadata); err == nil {
		fmt.Println(metadata)
	}
	return err
}

func doProcessParallel(inputFilePaths []string, errorFilesDir string, attributes domain.Attributes, successCollector processor.SuccessCollector) error {
	err := os.MkdirAll(errorFilesDir, os.ModePerm)
	if err != nil && !os.IsExist(err) {
		return errors.Wrapf(err, "cannot use %s as error files dir", errorFilesDir)
	}

	errorStream := make(chan error, 2)
	doneChannel := make(chan bool)
	processorWaitGroup := &sync.WaitGroup{}
	go handleErrors(errorStream, doneChannel)

	globalMetadata := processor.NewMetadata()
	metas := make([]processor.Metadata, len(inputFilePaths))
	for idx, inputCSVPath := range inputFilePaths {
		fileExtension := path.Ext(inputCSVPath)
		fileName := strings.ReplaceAll(path.Base(inputCSVPath), fileExtension, "")
		errorCSVPath := path.Join(errorFilesDir, fileName+"_errors.csv")
		metas[idx] = processor.NewMetadataWithTitle(fileName + fileExtension)
		processCSVAsync(inputCSVPath, errorCSVPath, attributes, successCollector, metas[idx], errorStream, processorWaitGroup)
	}

	processorWaitGroup.Wait()
	close(errorStream)
	<-doneChannel
	globalMetadata.AddDetails(metas...)
	fmt.Print(globalMetadata)
	return nil
}

func processCSVAsync(inputFilePath, errorFilePath string, attributes domain.Attributes, successCollector processor.SuccessCollector, metadata processor.Metadata, errorStream chan<- error, wg *sync.WaitGroup) {
	wg.Add(1)
	go func() {
		defer wg.Done()
		if err := processCSV(inputFilePath, errorFilePath, attributes, successCollector, metadata); err != nil {
			errorStream <- err
		}
	}()
}

func processCSV(inputFilePath, errorFilePath string, attributes domain.Attributes, successCollector processor.SuccessCollector, metadata processor.Metadata) error {
	inputReader, err := createCSVReader(inputFilePath)
	if err != nil {
		return err
	}
	errorCollector, err := createErrorCollector(inputReader.Header(), errorFilePath)
	if err != nil {
		return err
	}

	defer closeIO(errorCollector)

	dec, err := decoder.NewDecoder(attributes, inputReader.Header())
	if err != nil {
		return errors.Wrapf(err, "can not create decoder")
	}

	processor.New(inputReader, dec, successCollector.Collect, errorCollector.Collect).Process(metadata)
	return nil
}

func createCSVReader(inputFilePath string) (csv.Reader, error) {
	csvFile, err := csv.Open(inputFilePath)
	if err != nil {
		return nil, err
	}

	return csv.NewReader(csvFile)
}

func createErrorCollector(inputHeader []string, errorCSVPath string) (processor.ErrorCollector, error) {
	errorCSV, err := os.Create(errorCSVPath)
	if err != nil && !os.IsExist(err) {
		return nil, errors.Wrapf(err, "cannot create error output csv file")
	}

	return processor.NewErrorCollector(inputHeader, bufio.NewWriter(errorCSV))
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

func handlePanic() {
	err := recover()
	switch e := err.(type) {
	case error:
		log.Fatal().Err(e).Send()
	case string:
		log.Fatal().Msg(e)
	case nil:
		return
	default:
		log.Fatal().Msgf("error: %v", e)
	}
}

func closeIO(ioCloser io.Closer) {
	if err := ioCloser.Close(); err != nil {
		log.Error().Err(err).Msg("error while closing")
	}
}

func handleErrors(errorStream <-chan error, done chan bool) {
	for err := range errorStream {
		log.Error().Err(err).Send()
	}
	done <- true
}
