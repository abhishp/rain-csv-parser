package main

import (
	"os"

	"github.com/rs/zerolog/log"
	"github.com/urfave/cli/v2"
)

func main() {
	app := &cli.App{
		Name:    "rain-csv-parser",
		Version: version,
		Usage:   "A CSV parser to standardize the data from employers",
		Commands: cli.Commands{
			{
				Name:   "version",
				Usage:  "Show version of the parser",
				Action: printVersion,
			},
			{
				Name:      "process",
				Usage:     "process the csv file(s)",
				UsageText: "rain-csv-parser process [options] input.csv [input.csv...]",
				Flags: []cli.Flag{
					&cli.PathFlag{
						Name:    "config",
						Aliases: []string{"c"},
						Usage:   "use the specified config file",
						EnvVars: []string{"RAIN_CSV_PARSER_CONFIG"},
						Value:   "parser.yml",
					},
					cli.HelpFlag,
					&cli.BoolFlag{
						Name:    "debug",
						Aliases: []string{"d", "v"},
						Usage:   "enable debug output",
					},
					&cli.PathFlag{
						Name:    "success",
						Aliases: []string{"s"},
						Usage:   "set the success output file path",
						Value:   "success.csv",
					},
					&cli.PathFlag{
						Name:    "error",
						Aliases: []string{"e"},
						Usage:   "set the error output file path",
						Value:   "errors.csv",
					},

					&cli.PathFlag{
						Name:    "error-dir",
						Aliases: []string{"ed"},
						Usage:   "set the error output directory path when processing multiple files",
						Value:   "errors",
					},
				},
				Action:                 process,
				UseShortOptionHandling: true,
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Error().Err(err).Msg("process error")
		os.Exit(1)
	}
}
