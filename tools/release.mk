# RELEASE 		###########################################################################################

BUILD_DIR ?= ./build
BUILD_TOOLS_DIR ?= $(BUILD_DIR)/tools
SEMTAG ?= $(BUILD_TOOLS_DIR)/semtag

get-semtag:
	@mkdir -p $(BUILD_TOOLS_DIR)
	@curl -s 'https://raw.githubusercontent.com/nico2sh/semtag/master/semtag' > $(SEMTAG)
	@chmod a+x $(SEMTAG)

release-patch: get-semtag ##@release Releases a patch version
	$(SEMTAG) final -s patch

release-minor: get-semtag ##@release Releases a minor version
	$(SEMTAG) final -s minor

release-major: get-semtag ##@release Releases a major version
	$(SEMTAG) final -s major

version: get-semtag ##@release Get the last and current version
	$(SEMTAG) get

.PHONY: version
