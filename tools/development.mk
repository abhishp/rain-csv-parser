
get-ci-lint:
ifeq (, $(shell which golangci-lint))
	@{ \
		set -e ;\
		GOLANG_CI_LINTER_TMP_DIR="$$(mktemp -d)" ;\
		cd "$$GOLANG_CI_LINTER_TMP_DIR" ;\
		GO111MODULE=on go get github.com/golangci/golangci-lint/cmd/golangci-lint@v1.39.0; \
		rm -rf "$$GOLANG_CI_LINTER_TMP_DIR" ;\
	}
GOLANG_LINTER=$(GOBIN)/golangci-lint
else
GOLANG_LINTER=$(shell which golangci-lint)
endif

lint: get-ci-lint ##@development Runs linting using golangci-lint
	@echo "Running lint tools..."
	@go mod vendor
	$(GOLANG_LINTER) run --fix && $(GOLANG_LINTER) run

gotest:
ifeq (, $(shell which gotest))
	@{ \
		set -e ;\
		GOTEST_TMP_DIR="$$(mktemp -d)" ;\
		cd "$$GOTEST_TMP_DIR" ;\
		GO111MODULE=on go get github.com/rakyll/gotest; \
		rm -rf "$$GOTEST_TMP_DIR" ;\
	}
endif
