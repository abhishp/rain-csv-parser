package configs

import (
	"encoding/json"

	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

var config configs

func Init(configFile ...string) error {
	configuration := viper.New()
	configuration.AutomaticEnv()
	if len(configFile) == 0 || configFile[0] == "" {
		configuration.SetConfigName("parser")
		configuration.AddConfigPath("./")
		configuration.AddConfigPath("../")
		configuration.SetConfigType("yaml")
	} else {
		configuration.SetConfigFile(configFile[0])
	}

	switch err := configuration.ReadInConfig().(type) {
	case viper.ConfigFileNotFoundError:
	case nil:
	default:
		return errors.Wrapf(err, "invalid config file %s", configuration.ConfigFileUsed())
	}

	return initConfigs(configuration)
}

func initConfigs(configuration *viper.Viper) error {
	csvAttributes, err := mustGetJSON(configuration, "CSV.ATTRIBUTES")
	if err != nil {
		return err
	}

	config = configs{
		CSVAttributes: csvAttributes,
	}
	return nil
}

func mustGetJSON(configs *viper.Viper, key string) (json.RawMessage, error) {
	return json.RawMessage(configs.GetString(key)), validateKeyPresence(configs, key)
}

func validateKeyPresence(configs *viper.Viper, key string) error {
	if configs.Get(key) == nil {
		return errors.Errorf("missing config for %s", key)
	}
	return nil
}
