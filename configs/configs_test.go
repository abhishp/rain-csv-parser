package configs_test

import (
	"encoding/json"
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/suite"

	"gitlab.com/abhishp/rain-csv-parser/configs"
)

type ConfigsTestSuite struct {
	suite.Suite
}

func TestConfigs(t *testing.T) {
	suite.Run(t, new(ConfigsTestSuite))
}

func (suite *ConfigsTestSuite) SetupSuite() {
	if _, err := os.Stat("../parser.yml"); err == nil {
		err = os.Rename("../parser.yml", "../bak.parser.yml")
		suite.Require().Nil(err)
	}
}

func (suite *ConfigsTestSuite) TearDownSuite() {
	if _, err := os.Stat("../bak.parser.yml"); err == nil {
		err = os.Rename("../bak.parser.yml", "../parser.yml")
		suite.Require().Nil(err)
	}
}

func (suite *ConfigsTestSuite) TestInit() {
	suite.Run("return error when config file cannot be read", func() {
		configFile := "../test/.fixtures/configs/invalid_file_type.configs.json"
		err := configs.Init(configFile)

		suite.Require().NotNil(err)
		suite.Contains(err.Error(), fmt.Sprintf("invalid config file %s: ", configFile))
	})

	suite.Run("return invalid config error when non existent config file is given", func() {
		configFile := "../test/.fixtures/configs/does_not_exist.yml"
		err := configs.Init(configFile)

		suite.Require().NotNil(err)
		suite.Contains(err.Error(), fmt.Sprintf("invalid config file %[1]s: open %[1]s: no such file or directory", configFile))
	})

	suite.Run("return missing config error when no file is given", func() {
		err := configs.Init()

		suite.Require().NotNil(err)
		suite.Contains(err.Error(), "missing config for CSV.ATTRIBUTES")
	})

	suite.Run("return missing config error when empty config file string is given", func() {
		err := configs.Init("")

		suite.Require().NotNil(err)
		suite.Contains(err.Error(), "missing config for CSV.ATTRIBUTES")
	})

	suite.Run("return no errors for valid configs and populate configs object", func() {
		err := configs.Init("../test/.fixtures/configs/test.configs.yml")

		suite.Require().Nil(err)
	})
}

func (suite *ConfigsTestSuite) TestConfigs() {
	_ = configs.Init("../test/.fixtures/configs/test.configs.yml")

	suite.Run("csv attributes", func() {
		expectedAttributes := json.RawMessage(`[{"name":"name","outputHeader":"Name","inputHeaders":["First Name","Last Name"],"type":"string"},{"name":"emailId","outputHeader":"Email ID","inputHeaders":["Email"],"type":"email"},{"name":"salary","outputHeader":"Salary","inputHeaders":["Wage"],"type":"money"},{"name":"id","outputHeader":"ID","inputHeaders":["Number"],"type":"string"}]`)

		suite.Assert().Equal(expectedAttributes, configs.GetAttributes())
	})
}
